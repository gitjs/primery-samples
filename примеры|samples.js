
const ИМЯ = "Примеры GitJS";
const УКАЗАТЕЛЬ = "https://bitbucket.org/gitjs/primery-samples/raw/master/0000";
// // // //


ПроверитьЗапросПримера = function(мир)
{
    var запрос = decodeURI(window.location.search.substring(1));
    var аргументы = запрос.split("&");
    for (var номер in аргументы)
    {
        var аргумент = аргументы[номер];
        if (аргумент.startsWith("пример="))
        {
            мир.пример = аргумент.substring(7);
            мир.уведомить("есть запрос примера");
            return;
        }
    }
    мир.уведомить("нет запроса примера");
};


// // // //


ОтобразитьТаблицуПримеров = function(мир)
{
    var заголовок = `

<table border="1" cellspacing="0">
    <tr>
        <th>№</th>
        <th>Описание</th>
        <th>Description</th>
        <th>URL</th>
    </tr>

    `;

    var окончание = `

</table>

    `;

    var модуль = мир.модули.модульПоИмени(ИМЯ);
    var оглавление = JSON.parse(модуль.содержимое["/оглавление|toc.json"]);
    var содержимое = "";

    for (var номер in оглавление)
    {
        var запись = оглавление[номер];
        var счётчик = Number(номер) + 1;
        содержимое += `

<tr>
    <td>${счётчик}</td>
    <td>${запись.описание}</td>
    <td>${запись.description}</td>
    <td><a href="${запись.url}">URL</a></td>
</tr>

        `;
    }

    document.body.innerHTML = заголовок + содержимое + окончание;
};


// // // //


ЗапуститьПример = function(мир)
{
    document.body.innerHTML = `<p>Пуск примера: '${мир.пример}'</p>`;
    мир.уведомить(мир.пример);
};


// // // //


ПроверитьIFrame = function(мир)
{
    document.body.innerHTML = `

<iframe id="iframe1" src="http://gitjs.org?${УКАЗАТЕЛЬ}&пример=iframe1"></iframe>

<iframe id="iframe2" src="http://gitjs.org?${УКАЗАТЕЛЬ}&пример=iframe2"></iframe>

<button id="отправить">Отправить</button>

    `;

    var iframe1 = document.getElementById("iframe1");
    var iframe2 = document.getElementById("iframe2");
    var отправить = document.getElementById("отправить");
    отправить.onclick = function() {
        console.debug("отправить");
        iframe1.contentWindow.postMessage("Сообщение от родителя первому iframe", "*");
        iframe2.contentWindow.postMessage("Сообщение от родителя второму iframe", "*");
    };
};


// // // //


СоздатьДочернийIFrame1 = function(мир)
{
    document.body.innerHTML = `<p>Дочерний IFrame 1</p>`;

    function обработчик(событие)
    {
        document.body.innerHTML += `<p>Сообщение от родителя: '${событие.data}'</p>`;
    };

    window.addEventListener("message", обработчик, false);
};


// // // //


СоздатьДочернийIFrame2 = function(мир)
{
    document.body.innerHTML = `<p>Дочерний IFrame 2</p>`;

    function обработчик(событие)
    {
        document.body.innerHTML += `<p>Сообщение от родителя: '${событие.data}'</p>`;
    };

    window.addEventListener("message", обработчик, false);
};
